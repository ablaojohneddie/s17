// [SECTION] Introduction to Arrays

	//practice: create/declare multiple varables
	let student1 = 'Martin';
	let student2 = 'Maria';
	let student3 = 'John';
	let student4 = 'Ernie';
	let student5 = 'Bert';

	//store the following values inside a single container
	//to declare an aray => we simply use an 'array literal' or Square bracket '[]'
	//use '=' => assignment operator to repackage the stucture inside thevariables
	//be careful when selecting variables names.
	let batch = ['Martin', 'Maria', 'John', 'Ernie', 'Bert'];
	console.log(batch);

	//create an array that will contain diffeent computerBrands

	let computerBrands = ['Asus', 'Dell', 'Apple', 'Acer', 'Toshiba', 'Fujitsu'];
	console.log(computerBrands);

	// '' or " " => both are used to declare a string data type is JS.
	//[SIDE LESSON]

	//SYNTAX
	//=> 'apple' === "apples"
	// 'apple' //correct
	// "apple" //correct
	// 'apple" //wrong
	//if you use single quotations when declaring suh values like below, it will result to premarturely end the statement.
	let message = "Using Javascript's Array Literal";
	console.log(message);











